% automatyczne tworzenie wektora minimalnej obronnosci - sposob
% realizacji dowolny, zaleca sie by wartosci w kazdym punkcie byly wieksze
% od wektora wojsk poczatkowych
function [def_matrix] = create_def_req_forces()
size = get_SIZE_global;
matrix = zeros(1, size);
matrix(1) = 0;
for l = 2:get_SIZE_global
    switch mod(l,5) 
            case 0
                matrix(l) = 9;
            case 1
                matrix(l) = 12;
            case 2
                matrix(l) = 11;
            case 3
                matrix(l) = 17;
            case 4
                matrix(l) = 8;
            case 5
                matrix(l) = 16;
    end
end
def_matrix = matrix;
end

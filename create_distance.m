% automatyczne tworzenie macierzy odleglosci pomiedzy redutami - sposob
% realizacji dowolny
function [init_matrix] = create_distance()
size = get_SIZE_global;
matrix = zeros(size);

% Comment one:
% Random Generation

% for i = 1:size
%     for j = 1:size
%         if i == j
%         matrix(i,j) = 0;
%         else
%         matrix(i,j) = (1.5 * rand()); 
%         end
%     end
% end

% Always the same generation
for i = 1:size
        if i == 0
        matrix(1,i) = 0;
        else
        switch mod(i,5) 
            case 0
                matrix(1,i) = 2.2;
            case 1
                matrix(1,i) = 1.3;
            case 2
                matrix(1,i) = 2.4;
            case 3
                matrix(1,i) = 2.5;
            case 4
                matrix(1,i) = 0.1;
            case 5
                matrix(1,i) = 3.6;
        end
        end
end


init_matrix = matrix;
end
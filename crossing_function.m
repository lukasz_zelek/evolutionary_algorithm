function [pop_crossed_and_mutated] = crossing_function(pop_selected, init_forces)
   SIZE = size(pop_selected);
   Size = SIZE(1);
   SIZE_global = get_SIZE_global;
   POPULATION_SIZE = get_Global_POPULATION_SIZE;
   if Size == 0
       pop_crossed_and_mutated = pop_selected;
       return
   end
   new_generation = zeros(POPULATION_SIZE, SIZE_global);
   for i = 1:POPULATION_SIZE
       parent_1_index = randi(Size);
       parent_2_index = randi(Size);
       parent1 = pop_selected(parent_1_index, :);
       parent2 = pop_selected(parent_2_index, :);
       for j = 2:SIZE_global                                      % petla krzyzowania
           choose_parent = randi(2); 
           if choose_parent == 1
               new_generation(i, j) = parent1(j) - randi(6); % mutacja - odejmowanie losowej liczby
           else
               new_generation(i, j) = parent2(j) - randi(6);
           end           
       end
       % korekcja liczby jednostek - losowy wybor genow rodzicow moze
       % prowadzic do przekroczenia ilosci jednostek jakimi dysponowano w
       % zapleczu. Jednoczesnie realizuje to ii proces mutacji.
       Sum = sum(new_generation(i, :));
       if Sum ~= sum(init_forces)
           diff = Sum - sum(init_forces);
           if diff > 0
               while diff ~= 0
                   index = 1 + randi(SIZE_global - 1);
                   amount = randi(diff);
                   new_generation(i, index) = new_generation(i, index) - amount;
                   if new_generation(i, index) < init_forces(index)
                       new_generation(i, index) = new_generation(i, index) + amount;
                   else
                       diff = diff - amount;
                   end
                   
               end
           else
               while diff ~= 0
                   index = 1 + randi(SIZE_global - 1);
                   amount = randi(abs(diff));
                   new_generation(i, index) = new_generation(i, index) + amount;
                   diff = diff + amount;
               end
           end
       end
       
   end
   pop_crossed_and_mutated = new_generation;
end













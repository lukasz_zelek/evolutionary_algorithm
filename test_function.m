% funkcja pomocnicza - do testow czasu
function [duration] = test_function(size1, size2, size3)
    start_time = clock;
    global size_of_problem;
    global size_of_population;
    global number_of_iterations;
    size_of_problem = size1; 
    size_of_population = size2;
    number_of_iterations = size3;
    Ewolucja3;
    duration = etime(clock,start_time);
end


% funkcja testujaca - podlega modyfikacji w zaleznosci od potrzeb
% testowania
number_of_loop = 2;
global vector_of_solutions;
vector_of_solutions = zeros(number_of_loop, get_global_N);
vector_of_solutions_points_min = zeros(number_of_loop, get_global_N);
vector_of_solutions_points_max = zeros(number_of_loop, get_global_N);
vector_of_solutions_max = zeros(number_of_loop, get_global_N);
for z = 1:number_of_loop
 Ewolucja3;
 a = get_solution_num;
 vector_of_solutions(z,:) =  a(1,:);
 vector_of_solutions_max(z,:) =  a(4,:);
 vector_of_solutions_points_min(z, :) = a(2,:);
 vector_of_solutions_points_max(z, :) = a(3,:);
 vector_of_solutions(:,get_global_N) = [];
 vector_of_solutions_max(:,get_global_N) = [];
 vector_of_solutions_points_max(:, get_global_N)= [];
 x = linspace(1,get_global_N-1,get_global_N-1);
 figure(3)
 vector_of_solutions_points_min(:,end) = [];
 plot(x, vector_of_solutions_max(1,:), x,vector_of_solutions(1,:));
 xlabel("Iteracje"), ylabel("Ilo�� obronionych redut"), 
 title("Por�wnanie ilo�ci obronionych redut dla najlepszego i najgorszego osobnika populacji")
 legend("Najgorszy osobnik populacji", "Najlepszy osobnik populacji", 'Location','southeast');
 grid on;
 axis([0 get_global_N 0 17]);
 figure(2);
 plot(x, vector_of_solutions_points_min(1, :), x, vector_of_solutions_points_max(1, :));
 xlabel("Iteracje"), ylabel("Warto�� funckji celu"), 
 title("Por�wnanie warto�ci funkcji celu dla najlepszego i najgorszego osobnika populacji")
 legend("Najgorszy osobnik populacji", "Najlepszy osobnik populacji", 'Location','southeast');
 grid on;
 break;
end

% averge_first = mean(vector_of_solutions(:,1));
% averge_five = mean(vector_of_solutions(:,5));
% averge_ten = mean(vector_of_solutions(:,10));
% 
% min_first = mean(vector_of_solutions(:,1));
% min_five = mean(vector_of_solutions(:,5));
% min_ten = mean(vector_of_solutions(:,10));
% 
% max_first = mean(vector_of_solutions(:,1));
% max_five = mean(vector_of_solutions(:,5));
% max_ten = mean(vector_of_solutions(:,10));

% figure(2);
% x = linspace(1,get_global_N-1,get_global_N-1);
% % , x, vector_of_solutions(3,:), x, vector_of_solutions(4,:)
% plot(x, vector_of_solutions(1,:), x, vector_of_solutions(2,:));
% % xlabel("Iteracje"), ylabel("Minimalna ilo�� obronionych redut w populacji"), 
% % title("Wykresy przebieg�w r�nych rozwi�za� wraz z kolejnymi iteracjami")
% % legend("1 Wywo�anie", "2 Wywo�anie", "3 Wywo�anie", "4 wywo�anie", 'Location','southeast');
% figure(3)
% vector_of_solutions_points(:,end) = [];
% plot(x, vector_of_solutions_points(1,:), x, vector_of_solutions_points(2,:));
